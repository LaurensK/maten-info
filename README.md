# Maten-info - Mock API

Deze repository bevat een in javascript geschreven API om 'maten-info' op te kunnen slaan in de Mongo-database.
Tevens een op coc-restclient gebaseerde set aan api-request voorbeelden en een jMeter-JMX testplan.

## Locale installatie

### API + Database

1. Zorg dat Docker, Docker-compose, NodeJS en NPM geïnstalleerd zijn en werken.
2. Clone de repository
3. Navigeer naar de repository en run het commando:

   ```bash
   npm install --save
   ```

4. Start de database:

   ```bash
   docker-compose up -d
   ```

5. Ga in de browser naar `http://localhost:8081` en maak daar een 'collection' genaamd 'maten'.
6. Start de API:

   ```bash
   node server.js
   ```

7. De API zou nu bereikbaar moeten zijn op `http://localhost:5000/maten`. De browser zou een lege JSON-array
moeten tonen.

### JMX en Requests

1. de JMX kun openen met jMeter. Pas daar de 'User-defined-variables' naar wens aan.
2. de coc-restclient is een CoC-extension wat weer draait in Neovim/Vim. Grote kans dat VScode ook
   een restclient heeft. Misschien wel dezelfde.
