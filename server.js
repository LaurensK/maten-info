var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

var MATEN_COLLECTION = "maten";

var app = express();
app.use(bodyParser.json());

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI || "mongodb://maten:maten@localhost:27017/", function (err, client) {
   if (err) {
      console.log(err);
      process.exit(1);
   }

   // Save database object from the callback for reuse.
   db = client.db();
   console.log("Database connection ready");

   // Initialize the app.
   var server = app.listen(process.env.PORT || 5000, function () {
      var port = server.address().port;
      console.log("App now running on port", port);
   });
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
   console.log("ERROR: " + reason);
   res.status(code || 500).json({"error": message});
}

// Generic auth header checker
function checkAuth(req, res) {
   if (req.header("auth") == "ingelogd") {
      return true;
   } else {
      handleError(res, "UNAUTHORIZED", "Gebruiker niet ingelogd.", 403)
   }
   return false;
}

app.get("/maten", function (req, res) {
   if (checkAuth(req, res)) {
      db.collection(MATEN_COLLECTION).find({}).toArray(function (err, docs) {
         if (err) {
            handleError(res, err.message, "Failed to get maten.");
         } else {
            res.status(200).json(docs);
         }
      });
   }
});

app.post("/toevoegenMaat", function (req, res) {
   if (checkAuth(req, res)) {
      var newMaat = req.body;
      newMaat.createDate = new Date();

      if (!req.body.name) {
         handleError(res, "Invalid user input", "Must provide a name.", 400);
      } else {
         db.collection(MATEN_COLLECTION).insertOne(newMaat, function (err, doc) {
            if (err) {
               handleError(res, err.message, "Failed to create new maat.");
            } else {
               res.status(201).json(doc.ops[0]);
            }
         });
      }
   }
});

app.get("/maat/:id", function (req, res) {
   if (checkAuth(req, res)) {
      db.collection(MATEN_COLLECTION).findOne({_id: new ObjectID(req.params.id)}, function (err, doc) {
         if (err) {
            handleError(res, err.message, "Failed to get maat");
         } else {
            res.status(200).json(doc);
         }
      });
   }
});

app.put("/maat/:id", function (req, res) {
   if (checkAuth(req, res)) {
      var updateDoc = req.body;
      delete updateDoc._id;

      db.collection(MATEN_COLLECTION).replaceOne({_id: new ObjectID(req.params.id)}, updateDoc, function (err, doc) {
         if (err) {
            handleError(res, err.message, "Failed to update maat");
         } else {
            updateDoc._id = req.params.id;
            res.status(200).json(updateDoc);
         }
      });
   }
});

app.delete("/maat/:id", function (req, res) {
   if (checkAuth(req, res)) {
      db.collection(MATEN_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function (err, result) {
         if (err) {
            handleError(res, err.message, "Failed to delete maat");
         } else {
            res.status(200).json(req.params.id);
         }
      });
   }
});
